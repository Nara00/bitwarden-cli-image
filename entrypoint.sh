#!/bin/bash

set -e

bw config server ${BW_HOST}

export BW_USER=$BW_USER
export BW_PASSWORD=$BW_PASSWORD

export BW_SESSION=$(bw login ${BW_USER} --passwordenv BW_PASSWORD --raw)

echo BW_SESSION $BW_SESSION

bw unlock --check

echo 'Running `bw server` on port 8087'
bw serve --hostname all --disable-origin-protection
# bw serve --hostname 0.0.0.0 #--disable-origin-protection
